from analysis_tools import ObjectCollection, Category, Process, Dataset, Feature, Systematic
from analysis_tools.utils import DotDict
from analysis_tools.utils import join_root_selection as jrs
from plotting_tools import Label
from collections import OrderedDict

from config.run3_base_config import Config as base_config
from config.run3_dataset_2022_postEE import Datasets_2022_postEE as dataset_config

class Config_2022_postEE(base_config, dataset_config):
    def __init__(self, *args, **kwargs):
        super(Config_2022_postEE, self).__init__(*args, **kwargs)
        self = self.add_tau_id(self.year, "idDeepTau2018v2p5")
        self = self.add_bjet_id(self.year, self.runPeriod, "PNetB")
        self.regions = self.add_regions()
        self.categories = self.add_categories()
        self.systematics = self.add_systematics()
        self.features = self.add_features()

    def add_weights(self):
        weights = DotDict()
        weights.default = "1"
        weights.total_events_weights = ["genWeight", "puWeight"]

        # IMPORTANT:
        # If you don't want to use the DYstitching weight:
        #  - set the "stitchingNormalization" flag to "False" in the `run3_dataset_202*_pre/post*.py` configs
        #  - remove "DYstitchWeight" here below
        weights.corrections = ["genWeight", "puWeight", "trigSF", "DYstitchWeight",
                               "idAndIsoAndFakeSF", "bTagweightReshape"]

        # Baseline categories
        weights.base               = weights.corrections
        weights.baseline           = weights.corrections
        weights.baseline_bResolved = weights.corrections
        weights.baseline_bBoosted  = weights.corrections
        weights.base_selection     = weights.corrections
        weights.control            = weights.corrections

        # Analysis categories
        weights.res2b       = weights.corrections
        weights.res1b       = weights.corrections
        weights.boosted     = weights.corrections
        weights.vbf_2b      = weights.corrections
        weights.vbf_res2b   = weights.corrections
        weights.vbf_res1b   = weights.corrections
        weights.vbf_boosted = weights.corrections

        # VBF categories implemented as in Run2
        weights.vbf       = weights.corrections
        weights.vbf_loose = weights.corrections
        weights.vbf_tight = weights.corrections

        # CR categories
        weights.DY_CR_res1b          = weights.corrections
        weights.DY_CR_res2b          = weights.corrections
        weights.DY_CR_boosted        = weights.corrections
        weights.ttbar_invertedMassCR = weights.corrections

        return weights

    def add_default_module_files(self):
        defaults = {}
        defaults["PreprocessRDF"] = "run3_2022_preprocess_modulesrdf"
        defaults["Categorization"] = "run3_2022_postprocessHH_modulesrdf"
        defaults["PreCounter"] = "run3_precounter_weights"
        return defaults

config = Config_2022_postEE("Config_2022_postEE", year=2022, runPeriod="postEE", ecm=13.6, lumi_pb=26672)

