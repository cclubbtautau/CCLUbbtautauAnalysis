from analysis_tools import ObjectCollection, Category, Process, Dataset, Feature, Systematic
from analysis_tools.utils import DotDict
from analysis_tools.utils import join_root_selection as jrs
from plotting_tools import Label
from collections import OrderedDict

from cmt.config.base_config import Config as cmt_base_config

class Datasets_2022_postEE(cmt_base_config):
	def __init__(self, *args, **kwargs):
		super(Datasets_2022_postEE, self).__init__(*args, **kwargs)

	def add_datasets(self):
		datasets = [
			# Tau
			Dataset("Tau_2022E",
					dataset="/Tau/Run2022E-22Sep2023-v1/NANOAOD",
					process=self.processes.get("Tau_2022E"),
					runEra="E",
					tags=["NanoAODv12"]),

			Dataset("Tau_2022F",
					dataset="/Tau/Run2022F-22Sep2023-v1/NANOAOD",
					process=self.processes.get("Tau_2022F"),
					runEra="F",
					tags=["NanoAODv12"]),

			Dataset("Tau_2022G",
					dataset="/Tau/Run2022G-22Sep2023-v1/NANOAOD",
					process=self.processes.get("Tau_2022G"),
					runEra="G",
					tags=["NanoAODv12"]),

			# Muon
			Dataset("Muon_2022E",
					dataset="/Muon/Run2022E-22Sep2023-v1/NANOAOD",
					process=self.processes.get("Muon_2022E"),
					runEra="E",
					tags=["NanoAODv12"]),

			Dataset("Muon_2022F",
					dataset="/Muon/Run2022F-22Sep2023-v2/NANOAOD",
					process=self.processes.get("Muon_2022F"),
					runEra="F",
					tags=["NanoAODv12"]),

			Dataset("Muon_2022G",
					dataset="/Muon/Run2022G-22Sep2023-v1/NANOAOD",
					process=self.processes.get("Muon_2022G"),
					runEra="G",
					tags=["NanoAODv12"]),

			# EGamma
			Dataset("EGamma_2022E",
					dataset="/EGamma/Run2022E-22Sep2023-v1/NANOAOD",
					process=self.processes.get("EGamma_2022E"),
					runEra="E",
					tags=["NanoAODv12"]),

			Dataset("EGamma_2022F",
					dataset="/EGamma/Run2022F-22Sep2023-v1/NANOAOD",
					process=self.processes.get("EGamma_2022F"),
					runEra="F",
					tags=["NanoAODv12"]),

			Dataset("EGamma_2022G",
					dataset="/EGamma/Run2022G-22Sep2023-v2/NANOAOD",
					process=self.processes.get("EGamma_2022G"),
					runEra="G",
					tags=["NanoAODv12"]),

			# JetMET
			Dataset("JetMET_2022E",
					dataset="/JetMET/Run2022E-22Sep2023-v1/NANOAOD",
					process=self.processes.get("JetMET_2022E"),
					runEra="E",
					tags=["NanoAODv12"]),

			Dataset("JetMET_2022F",
					dataset="/JetMET/Run2022F-22Sep2023-v2/NANOAOD",
					process=self.processes.get("JetMET_2022F"),
					runEra="F",
					tags=["NanoAODv12"]),

			Dataset("JetMET_2022G",
					dataset="/JetMET/Run2022G-22Sep2023-v2/NANOAOD",
					process=self.processes.get("JetMET_2022G"),
					runEra="G",
					tags=["NanoAODv12"]),

			# DY
			# Do not use - please use the "ext1" sample
			#Dataset("DYto2L-2Jets_MLL-50",
			#		dataset="/DYto2L-2Jets_MLL-50_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
			#		process=self.processes.get("DYto2L-2Jets_MLL-50"),
			#		xs=6282.6,
			#		tags=["NanoAODv12"]),

			# IMPORTANT:
			# If you don't want to use the stitching (and only use the DYincl sample):
			#  - remove "DYstitchWeight" from the `run3_202*_pre/post*.py` config
			#  - set the "stitchingNormalization" flag to "False" here below
			Dataset("DYto2L-2Jets_MLL-50_ext1",
					dataset="/DYto2L-2Jets_MLL-50_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v1/NANOAODSIM",
					process=self.processes.get("DYto2L-2Jets_MLL-50_ext1"),
					xs=6282.6,
					stitchingNormalization=True,
					tags=["NanoAODv12"]),

			Dataset("DYto2L-2Jets_MLL-50_0J",
					dataset="/DYto2L-2Jets_MLL-50_0J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("DYto2L-2Jets_MLL-50_0J"),
					xs=6282.6,
					stitchingNormalization=True,
					tags=["NanoAODv12"]),

			Dataset("DYto2L-2Jets_MLL-50_1J",
					dataset="/DYto2L-2Jets_MLL-50_1J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("DYto2L-2Jets_MLL-50_1J"),
					xs=6282.6,
					stitchingNormalization=True,
					tags=["NanoAODv12"]),

			Dataset("DYto2L-2Jets_MLL-50_2J",
					dataset="/DYto2L-2Jets_MLL-50_2J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("DYto2L-2Jets_MLL-50_2J"),
					xs=6282.6,
					stitchingNormalization=True,
					tags=["NanoAODv12"]),

			Dataset("DYto2L-2Jets_MLL-50_PTLL-40to100_1J",
					dataset="/DYto2L-2Jets_MLL-50_PTLL-40to100_1J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v1/NANOAODSIM",
					process=self.processes.get("DYto2L-2Jets_MLL-50_PTLL-40to100_1J"),
					xs=6282.6,
					stitchingNormalization=True,
					tags=["NanoAODv12"]),

			Dataset("DYto2L-2Jets_MLL-50_PTLL-40to100_2J",
					dataset="/DYto2L-2Jets_MLL-50_PTLL-40to100_2J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v3/NANOAODSIM",
					process=self.processes.get("DYto2L-2Jets_MLL-50_PTLL-40to100_2J"),
					xs=6282.6,
					stitchingNormalization=True,
					tags=["NanoAODv12"]),

			Dataset("DYto2L-2Jets_MLL-50_PTLL-100to200_1J",
					dataset="/DYto2L-2Jets_MLL-50_PTLL-100to200_1J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v1/NANOAODSIM",
					process=self.processes.get("DYto2L-2Jets_MLL-50_PTLL-100to200_1J"),
					xs=6282.6,
					stitchingNormalization=True,
					tags=["NanoAODv12"]),

			Dataset("DYto2L-2Jets_MLL-50_PTLL-100to200_2J",
					dataset="/DYto2L-2Jets_MLL-50_PTLL-100to200_2J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v3/NANOAODSIM",
					process=self.processes.get("DYto2L-2Jets_MLL-50_PTLL-100to200_2J"),
					xs=6282.6,
					stitchingNormalization=True,
					tags=["NanoAODv12"]),

			Dataset("DYto2L-2Jets_MLL-50_PTLL-200to400_1J",
					dataset="/DYto2L-2Jets_MLL-50_PTLL-200to400_1J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v1/NANOAODSIM",
					process=self.processes.get("DYto2L-2Jets_MLL-50_PTLL-200to400_1J"),
					xs=6282.6,
					stitchingNormalization=True,
					tags=["NanoAODv12"]),

			Dataset("DYto2L-2Jets_MLL-50_PTLL-200to400_2J",
					dataset="/DYto2L-2Jets_MLL-50_PTLL-200to400_2J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v3/NANOAODSIM",
					process=self.processes.get("DYto2L-2Jets_MLL-50_PTLL-200to400_2J"),
					xs=6282.6,
					stitchingNormalization=True,
					tags=["NanoAODv12"]),

			Dataset("DYto2L-2Jets_MLL-50_PTLL-400to600_1J",
					dataset="/DYto2L-2Jets_MLL-50_PTLL-400to600_1J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v1/NANOAODSIM",
					process=self.processes.get("DYto2L-2Jets_MLL-50_PTLL-400to600_1J"),
					xs=6282.6,
					stitchingNormalization=True,
					tags=["NanoAODv12"]),

			Dataset("DYto2L-2Jets_MLL-50_PTLL-400to600_2J",
					dataset="/DYto2L-2Jets_MLL-50_PTLL-400to600_2J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("DYto2L-2Jets_MLL-50_PTLL-400to600_2J"),
					xs=6282.6,
					stitchingNormalization=True,
					tags=["NanoAODv12"]),

			Dataset("DYto2L-2Jets_MLL-50_PTLL-600_1J",
					dataset="/DYto2L-2Jets_MLL-50_PTLL-600_1J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v3/NANOAODSIM",
					process=self.processes.get("DYto2L-2Jets_MLL-50_PTLL-600_1J"),
					xs=6282.6,
					stitchingNormalization=True,
					tags=["NanoAODv12"]),

			Dataset("DYto2L-2Jets_MLL-50_PTLL-600_2J",
					dataset="/DYto2L-2Jets_MLL-50_PTLL-600_2J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v1/NANOAODSIM",
					process=self.processes.get("DYto2L-2Jets_MLL-50_PTLL-600_2J"),
					xs=6282.6,
					stitchingNormalization=True,
					tags=["NanoAODv12"]),

			# Di-boson
			Dataset("WWto2L2Nu",
					dataset="/WWto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("WWto2L2Nu"),
					xs=12.9800,
					tags=["NanoAODv12"]),

			Dataset("WWto2L2Nu_ext1",
					dataset="/WWto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v2/NANOAODSIM",
					process=self.processes.get("WWto2L2Nu_ext1"),
					xs=12.9800,
					tags=["NanoAODv12"]),

			Dataset("WWtoLNu2Q",
					dataset="/WWtoLNu2Q_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("WWtoLNu2Q"),
					xs=53.7128,
					tags=["NanoAODv12"]),

			Dataset("WWtoLNu2Q_ext1",
					dataset="/WWtoLNu2Q_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v2/NANOAODSIM",
					process=self.processes.get("WWtoLNu2Q_ext1"),
					xs=53.7128,
					tags=["NanoAODv12"]),

			Dataset("WWto4Q",
					dataset="/WWto4Q_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("WWto4Q"),
					xs=55.5675,
					tags=["NanoAODv12"]),

			Dataset("WWto4Q_ext1",
					dataset="/WWto4Q_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v2/NANOAODSIM",
					process=self.processes.get("WWto4Q_ext1"),
					xs=55.5675,
					tags=["NanoAODv12"]),

			Dataset("ZZto2L2Nu",
					dataset="/ZZto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("ZZto2L2Nu"),
					xs=1.1960,
					tags=["NanoAODv12"]),

			Dataset("ZZto2L2Nu_ext1",
					dataset="/ZZto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v2/NANOAODSIM",
					process=self.processes.get("ZZto2L2Nu_ext1"),
					xs=1.1960,
					tags=["NanoAODv12"]),

			Dataset("ZZto2L2Q",
					dataset="/ZZto2L2Q_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("ZZto2L2Q"),
					xs=8.0777,
					tags=["NanoAODv12"]),

			Dataset("ZZto2L2Q_ext1",
					dataset="/ZZto2L2Q_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v2/NANOAODSIM",
					process=self.processes.get("ZZto2L2Q_ext1"),
					xs=8.0777,
					tags=["NanoAODv12"]),

			Dataset("ZZto2Nu2Q",
					dataset="/ZZto2Nu2Q_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("ZZto2Nu2Q"),
					xs=5.5982,
					tags=["NanoAODv12"]),

			Dataset("ZZto2Nu2Q_ext1",
					dataset="/ZZto2Nu2Q_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v2/NANOAODSIM",
					process=self.processes.get("ZZto2Nu2Q_ext1"),
					xs=5.5982,
					tags=["NanoAODv12"]),

			Dataset("ZZto4L",
					dataset="/ZZto4L_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("ZZto4L"),
					xs=1.6541,
					tags=["NanoAODv12"]),

			Dataset("ZZto4L_ext1",
					dataset="/ZZto4L_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v2/NANOAODSIM",
					process=self.processes.get("ZZto4L_ext1"),
					xs=1.6541,
					tags=["NanoAODv12"]),

			Dataset("WZto2L2Q",
					dataset="/WZto2L2Q_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("WZto2L2Q"),
					xs=8.1734,
					tags=["NanoAODv12"]),

			Dataset("WZtoLNu2Q",
					dataset="/WZtoLNu2Q_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("WZtoLNu2Q"),
					xs=17.1396,
					tags=["NanoAODv12"]),

			Dataset("WZto3LNu",
					dataset="/WZto3LNu_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("WZto3LNu"),
					xs=5.3179,
					tags=["NanoAODv12"]),

			# Tri-boson
			Dataset("WWW",
					dataset="/WWW_4F_TuneCP5_13p6TeV_amcatnlo-madspin-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("WWW"),
					xs=0.23280,
					tags=["NanoAODv12"]),

			Dataset("WWZ",
					dataset="/WWZ_4F_TuneCP5_13p6TeV_amcatnlo-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("WWZ"),
					xs=0.18510,
					tags=["NanoAODv12"]),

			Dataset("WZZ",
					dataset="/WZZ_TuneCP5_13p6TeV_amcatnlo-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("WZZ"),
					xs=0.06206,
					tags=["NanoAODv12"]),

			Dataset("ZZZ",
					dataset="/ZZZ_TuneCP5_13p6TeV_amcatnlo-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("ZZZ"),
					xs=0.01591,
					tags=["NanoAODv12"]),

			# Single Top
			Dataset("TWminusto2L2Nu",
					dataset=[
						"/TWminusto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
						"/TWminusto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v2/NANOAODSIM"
					],
					process=self.processes.get("TWminusto2L2Nu"),
					xs=4.6651,
					tags=["NanoAODv12"]),

			Dataset("TWminustoLNu2Q",
					dataset=[
						"/TWminustoLNu2Q_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
						"/TWminustoLNu2Q_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v2/NANOAODSIM"
					],
					process=self.processes.get("TWminustoLNu2Q"),
					xs=19.3048,
					tags=["NanoAODv12"]),

			Dataset("TWminusto4Q",
					dataset=[
						"/TWminusto4Q_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
						"/TWminusto4Q_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v2/NANOAODSIM"
					],
					process=self.processes.get("TWminusto4Q"),
					xs=19.9714,
					tags=["NanoAODv12"]),

			Dataset("TbarWplusto2L2Nu",
					dataset=[
						"/TbarWplusto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
						"/TbarWplusto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v2/NANOAODSIM"
					],
					process=self.processes.get("TbarWplusto2L2Nu"),
					xs=4.6651,
					tags=["NanoAODv12"]),


			Dataset("TbarWplustoLNu2Q",
					dataset=[
						"/TbarWplustoLNu2Q_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
						"/TbarWplustoLNu2Q_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v2/NANOAODSIM"
					],
					process=self.processes.get("TbarWplustoLNu2Q"),
					xs=19.3048,
					tags=["NanoAODv12"]),

			Dataset("TbarWplusto4Q",
					dataset=[
						"/TbarWplusto4Q_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
						"/TbarWplusto4Q_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v2/NANOAODSIM"
					],
					process=self.processes.get("TbarWplusto4Q"),
					xs=19.9714,
					tags=["NanoAODv12"]),

			Dataset("TBbarQ_t-channel",
					dataset="/TBbarQ_t-channel_4FS_TuneCP5_13p6TeV_powheg-madspin-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("TBbarQ_t-channel"),
					xs=145.00,
					tags=["NanoAODv12"]),

			Dataset("TbarBQ_t-channel",
					dataset="/TbarBQ_t-channel_4FS_TuneCP5_13p6TeV_powheg-madspin-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("TbarBQ_t-channel"),
					xs=87.200,
					tags=["NanoAODv12"]),

			Dataset("TBbartoLplusNuBbar-s-channel",
					dataset="/TBbartoLplusNuBbar-s-channel-4FS_TuneCP5_13p6TeV_amcatnlo-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("TBbartoLplusNuBbar-s-channel"),
					xs=2.3601,
					tags=["NanoAODv12"]),

			Dataset("TbarBtoLminusNuB-s-channel",
					dataset="/TbarBtoLminusNuB-s-channel-4FS_TuneCP5_13p6TeV_amcatnlo-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("TbarBtoLminusNuB-s-channel"),
					xs=1.4772,
					tags=["NanoAODv12"]),

			# ttH, TT+Di-boson
			Dataset("TTHto2B",
					dataset="/TTHto2B_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v3/NANOAODSIM",
					process=self.processes.get("TTHto2B"),
					xs=0.33197,
					tags=["NanoAODv12"]),

			Dataset("TTHtoNon2B",
					dataset="/TTHtoNon2B_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("TTHtoNon2B"),
					xs=0.23803,
					tags=["NanoAODv12"]),

			Dataset("TTWH",
					dataset="/TTWH_TuneCP5_13p6TeV_madgraph-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("TTWH"),
					xs=0.00125,
					tags=["NanoAODv12"]),

			Dataset("TTWW",
					dataset="/TTWW_TuneCP5_13p6TeV_madgraph-madspin-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("TTWW"),
					xs=0.00817,
					tags=["NanoAODv12"]),

			Dataset("TTZH",
					dataset="/TTZH_TuneCP5_13p6TeV_madgraph-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v3/NANOAODSIM",
					process=self.processes.get("TTZH"),
					xs=0.00129,
					tags=["NanoAODv12"]),

			Dataset("TTZZ",
					dataset="/TTZZ_TuneCP5_13p6TeV_madgraph-madspin-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v3/NANOAODSIM",
					process=self.processes.get("TTZZ"),
					xs=0.00156,
					tags=["NanoAODv12"]),

			Dataset("TTWZ",
					dataset="/TTWZ_TuneCP5_13p6TeV_madgraph-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("TTWZ"),
					xs=0.00277,
					tags=["NanoAODv12"]),

			# Wjets
			Dataset("WtoLNu-2Jets",
					dataset="/WtoLNu-2Jets_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("WtoLNu-2Jets"),
					xs=63425.100,
					tags=["NanoAODv12"]),

			Dataset("WtoLNu-2Jets_PTLNu-40to100_1J",
					dataset="/WtoLNu-2Jets_PTLNu-40to100_1J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v3/NANOAODSIM",
					process=self.processes.get("WtoLNu-2Jets_PTLNu-40to100_1J"),
					xs=4982.8203,
					tags=["NanoAODv12"]),

			Dataset("WtoLNu-2Jets_PTLNu-40to100_2J",
					dataset="/WtoLNu-2Jets_PTLNu-40to100_2J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v3/NANOAODSIM",
					process=self.processes.get("WtoLNu-2Jets_PTLNu-40to100_2J"),
					xs=1798.6327,
					tags=["NanoAODv12"]),

			Dataset("WtoLNu-2Jets_PTLNu-100to200_1J",
					dataset="/WtoLNu-2Jets_PTLNu-100to200_1J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v3/NANOAODSIM",
					process=self.processes.get("WtoLNu-2Jets_PTLNu-100to200_1J"),
					xs=414.4284,
					tags=["NanoAODv12"]),

			Dataset("WtoLNu-2Jets_PTLNu-100to200_2J",
					dataset="/WtoLNu-2Jets_PTLNu-100to200_2J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v3/NANOAODSIM",
					process=self.processes.get("WtoLNu-2Jets_PTLNu-100to200_2J"),
					xs=474.8705,
					tags=["NanoAODv12"]),

			Dataset("WtoLNu-2Jets_PTLNu-200to400_1J",
					dataset="/WtoLNu-2Jets_PTLNu-200to400_1J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v3/NANOAODSIM",
					process=self.processes.get("WtoLNu-2Jets_PTLNu-200to400_1J"),
					xs=28.8141,
					tags=["NanoAODv12"]),

			Dataset("WtoLNu-2Jets_PTLNu-200to400_2J",
					dataset="/WtoLNu-2Jets_PTLNu-200to400_2J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v1/NANOAODSIM",
					process=self.processes.get("WtoLNu-2Jets_PTLNu-200to400_2J"),
					xs=61.6465,
					tags=["NanoAODv12"]),

			Dataset("WtoLNu-2Jets_PTLNu-400to600_1J",
					dataset="/WtoLNu-2Jets_PTLNu-400to600_1J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v1/NANOAODSIM",
					process=self.processes.get("WtoLNu-2Jets_PTLNu-400to600_1J"),
					xs=0.9888,
					tags=["NanoAODv12"]),

			Dataset("WtoLNu-2Jets_PTLNu-400to600_2J",
					dataset="/WtoLNu-2Jets_PTLNu-400to600_2J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v1/NANOAODSIM",
					process=self.processes.get("WtoLNu-2Jets_PTLNu-400to600_2J"),
					xs=3.5106,
					tags=["NanoAODv12"]),

			Dataset("WtoLNu-2Jets_PTLNu-600_1J",
					dataset="/WtoLNu-2Jets_PTLNu-600_1J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v1/NANOAODSIM",
					process=self.processes.get("WtoLNu-2Jets_PTLNu-600_1J"),
					xs=0.1185,
					tags=["NanoAODv12"]),

			Dataset("WtoLNu-2Jets_PTLNu-600_2J",
					dataset="/WtoLNu-2Jets_PTLNu-600_2J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v1/NANOAODSIM",
					process=self.processes.get("WtoLNu-2Jets_PTLNu-600_2J"),
					xs=0.5922,
					tags=["NanoAODv12"]),

			# Currently not used
			#Dataset("WtoLNu-2Jets_0J",
			#		dataset="/WtoLNu-2Jets_0J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v3/NANOAODSIM",
			#		process=self.processes.get("WtoLNu-2Jets_0J"),
			#		xs=1.0,
			#		tags=["NanoAODv12"]),

			#Dataset("WtoLNu-2Jets_1J",
			#		dataset="/WtoLNu-2Jets_1J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
			#		process=self.processes.get("WtoLNu-2Jets_1J"),
			#		xs=1.0,
			#		tags=["NanoAODv12"]),

			#Dataset("WtoLNu-2Jets_2J",
			#		dataset="/WtoLNu-2Jets_2J_TuneCP5_13p6TeV_amcatnloFXFX-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
			#		process=self.processes.get("WtoLNu-2Jets_2J"),
			#		xs=1.0,
			#		tags=["NanoAODv12"]),

			#Dataset("WtoLNu-4Jets",
			#		dataset="/WtoLNu-4Jets_TuneCP5_13p6TeV_madgraphMLM-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
			#		process=self.processes.get("WtoLNu-4Jets"),
			#		xs=1.0,
			#		tags=["NanoAODv12"]),

			#Dataset("WtoLNu-4Jets_ext1",
			#		dataset="/WtoLNu-4Jets_TuneCP5_13p6TeV_madgraphMLM-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v2/NANOAODSIM",
			#		process=self.processes.get("WtoLNu-4Jets_ext1"),
			#		xs=1.0,
			#		tags=["NanoAODv12"]),

			#Dataset("WtoLNu-4Jets_1J",
			#		dataset="/WtoLNu-4Jets_1J_TuneCP5_13p6TeV_madgraphMLM-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
			#		process=self.processes.get("WtoLNu-4Jets_1J"),
			#		xs=1.0,
			#		tags=["NanoAODv12"]),

			#Dataset("WtoLNu-4Jets_2J",
			#		dataset="/WtoLNu-4Jets_2J_TuneCP5_13p6TeV_madgraphMLM-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
			#		process=self.processes.get("WtoLNu-4Jets_2J"),
			#		xs=1.0,
			#		tags=["NanoAODv12"]),

			#Dataset("WtoLNu-4Jets_3J",
			#		dataset="/WtoLNu-4Jets_3J_TuneCP5_13p6TeV_madgraphMLM-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
			#		process=self.processes.get("WtoLNu-4Jets_3J"),
			#		xs=1.0,
			#		tags=["NanoAODv12"]),

			#Dataset("WtoLNu-4Jets_4J",
			#		dataset="/WtoLNu-4Jets_4J_TuneCP5_13p6TeV_madgraphMLM-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
			#		process=self.processes.get("WtoLNu-4Jets_4J"),
			#		xs=1.0,
			#		tags=["NanoAODv12"]),

			# Single Higgs->TauTau/BB
			Dataset("GluGluHToTauTau",
					dataset="/GluGluHTo2TauUncorrelatedDecay_Filtered_M-125_CP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("GluGluHToTauTau"),
					xs=1.26703,
					tags=["NanoAODv12"]),

			Dataset("GluGluHto2B",
					dataset="/GluGluHto2B_M-125_TuneCP5_13p6TeV_powheg-minlo-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("GluGluHto2B"),
					xs=30.41875,
					tags=["NanoAODv12"]),

			Dataset("VBFHToTauTau",
					dataset="/VBFHTo2TauUncorrelatedDecay_Filtered_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("VBFHToTauTau"),
					xs=0.10780,
					tags=["NanoAODv12"]),

			Dataset("VBFHto2B",
					dataset="/VBFHto2B_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v3/NANOAODSIM",
					process=self.processes.get("VBFHto2B"),
					xs=2.37503,
					tags=["NanoAODv12"]),

			Dataset("WminusHTo2Tau",
					dataset="/WminusHTo2TauUncorrelatedDecay_Filtered_M-125_TuneCP5_13p6TeV_powheg-minnlo-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("WminusHTo2Tau"),
					xs=0.03534,
					tags=["NanoAODv12"]),

			Dataset("WplusHTo2Tau",
					dataset="/WplusHTo2TauUncorrelatedDecay_Filtered_M-125_TuneCP5_13p6TeV_powheg-minnlo-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("WplusHTo2Tau"),
					xs=0.03272,
					tags=["NanoAODv12"]),

			Dataset("ZHto2Tau",
					dataset="/ZHto2TauUncorrelatedDecay_Filtered_M-125_CP5_13p6TeV_powheg-minnlo-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("ZHto2Tau"),
					xs=0.02385,
					tags=["NanoAODv12"]),

			# ZH
			Dataset("ZH_Hto2B_Zto2L",
					dataset="/ZH_Hto2B_Zto2L_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("ZH_Hto2B_Zto2L"),
					xs=0.05551,
					tags=["NanoAODv12"]),

			Dataset("ZH_Hto2B_Zto2L_ext1",
					dataset="/ZH_Hto2B_Zto2L_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v2/NANOAODSIM",
					process=self.processes.get("ZH_Hto2B_Zto2L_ext1"),
					xs=0.05551,
					tags=["NanoAODv12"]),

			Dataset("ZH_Hto2B_Zto2Q",
					dataset="/ZH_Hto2B_Zto2Q_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("ZH_Hto2B_Zto2Q"),
					xs=0.38432,
					tags=["NanoAODv12"]),

			Dataset("ZH_Hto2B_Zto2Q_ext1",
					dataset="/ZH_Hto2B_Zto2Q_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v3/NANOAODSIM",
					process=self.processes.get("ZH_Hto2B_Zto2Q_ext1"),
					xs=0.38432,
					tags=["NanoAODv12"]),

			# WH
			Dataset("WminusH_Hto2B_Wto2Q",
					dataset="/WminusH_Hto2B_Wto2Q_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("WminusH_Hto2B_Wto2Q"),
					xs=0.57201,
					tags=["NanoAODv12"]),

			Dataset("WminusH_Hto2B_Wto2Q_ext1",
					dataset="/WminusH_Hto2B_Wto2Q_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v2/NANOAODSIM",
					process=self.processes.get("WminusH_Hto2B_Wto2Q_ext1"),
					xs=0.57201,
					tags=["NanoAODv12"]),

			Dataset("WminusH_Hto2B_WtoLNu",
					dataset="/WminusH_Hto2B_WtoLNu_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("WminusH_Hto2B_WtoLNu"),
					xs=0.27646,
					tags=["NanoAODv12"]),

			Dataset("WminusH_Hto2B_WtoLNu_ext1",
					dataset="/WminusH_Hto2B_WtoLNu_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v2/NANOAODSIM",
					process=self.processes.get("WminusH_Hto2B_WtoLNu_ext1"),
					xs=0.27646,
					tags=["NanoAODv12"]),

			Dataset("WplusH_Hto2B_Wto2Q",
					dataset="/WplusH_Hto2B_Wto2Q_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("WplusH_Hto2B_Wto2Q"),
					xs=0.57201,
					tags=["NanoAODv12"]),

			Dataset("WplusH_Hto2B_Wto2Q_ext1",
					dataset="/WplusH_Hto2B_Wto2Q_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v2/NANOAODSIM",
					process=self.processes.get("WplusH_Hto2B_Wto2Q_ext1"),
					xs=0.57201,
					tags=["NanoAODv12"]),

			Dataset("WplusH_Hto2B_WtoLNu",
					dataset="/WplusH_Hto2B_WtoLNu_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("WplusH_Hto2B_WtoLNu"),
					xs=0.27646,
					tags=["NanoAODv12"]),

			Dataset("WplusH_Hto2B_WtoLNu_ext1",
					dataset="/WplusH_Hto2B_WtoLNu_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v2/NANOAODSIM",
					process=self.processes.get("WplusH_Hto2B_WtoLNu_ext1"),
					xs=0.27646,
					tags=["NanoAODv12"]),

			# ggZH
			Dataset("ggZH_Hto2B_Zto2L",
					dataset="/ggZH_Hto2B_Zto2L_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("ggZH_Hto2B_Zto2L"),
					xs=0.00800,
					tags=["NanoAODv12"]),

			Dataset("ggZH_Hto2B_Zto2L_ext1",
					dataset="/ggZH_Hto2B_Zto2L_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v3/NANOAODSIM",
					process=self.processes.get("ggZH_Hto2B_Zto2L_ext1"),
					xs=0.00800,
					tags=["NanoAODv12"]),

			Dataset("ggZH_Hto2B_Zto2Q",
					dataset="/ggZH_Hto2B_Zto2Q_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("ggZH_Hto2B_Zto2Q"),
					xs=0.05537,
					tags=["NanoAODv12"]),

			Dataset("ggZH_Hto2B_Zto2Q_ext1",
					dataset="/ggZH_Hto2B_Zto2Q_M-125_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v3/NANOAODSIM",
					process=self.processes.get("ggZH_Hto2B_Zto2Q_ext1"),
					xs=0.05537,
					tags=["NanoAODv12"]),

			# TT
			Dataset("TTto2L2Nu",
					dataset=[
						"/TTto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
						"/TTto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v2/NANOAODSIM"
					],
					process=self.processes.get("TTto2L2Nu"),
					xs=98.036113,
					merging={"base": 10, "baseline": 10, "baseline_bResolved": 10, "baseline_bBoosted": 10, "mutau": 10, "etau": 10, "tautau": 10,
							 "resolved_1b": 10, "resolved_2b": 10, "boosted": 10, "vbf_loose": 10, "vbf_tight": 10, "vbf": 10, "ttbar_invertedMassCR": 10},
					tags=["NanoAODv12"]),

			Dataset("TTtoLNu2Q",
					dataset=[
						"/TTtoLNu2Q_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
						"/TTtoLNu2Q_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v2/NANOAODSIM"
					],
					process=self.processes.get("TTtoLNu2Q"),
					xs=405.685352,
					merging={"base": 10, "baseline": 10, "baseline_bResolved": 10, "baseline_bBoosted": 10, "mutau": 10, "etau": 10, "tautau": 10,
							 "resolved_1b": 10, "resolved_2b": 10, "boosted": 10, "vbf_loose": 10, "vbf_tight": 10, "vbf": 10, "ttbar_invertedMassCR": 10},
					tags=["NanoAODv12"]),

			Dataset("TTto4Q",
					dataset=[
						"/TTto4Q_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
						"/TTto4Q_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6_ext1-v2/NANOAODSIM"
					],
					process=self.processes.get("TTto4Q"),
					merging={"base": 10, "baseline": 10, "baseline_bResolved": 10, "baseline_bBoosted": 10, "mutau": 10, "etau": 10, "tautau": 10,
							 "resolved_1b": 10, "resolved_2b": 10, "boosted": 10, "vbf_loose": 10, "vbf_tight": 10, "vbf": 10, "ttbar_invertedMassCR": 10},
					xs=419.693824,
					tags=["NanoAODv12"]),

			# Non-resonant VBFHH signals
			Dataset("VBFHHto2B2Tau_CV-1_C2V-1_C3-1",
					dataset="/VBFHHto2B2Tau_CV_1_C2V_1_C3_1_TuneCP5_13p6TeV_madgraph-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("VBFHHto2B2Tau_CV-1_C2V-1_C3-1"),
					xs=0.0001415778,
					tags=["NanoAODv12"]),

			Dataset("VBFHHto2B2Tau_CV-1_C2V-0_C3-1",
					dataset="/VBFHHto2B2Tau_CV_1_C2V_0_C3_1_TuneCP5_13p6TeV_madgraph-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("VBFHHto2B2Tau_CV-1_C2V-0_C3-1"),
					xs=0.0021778912,
					tags=["NanoAODv12"]),

			Dataset("VBFHHto2B2Tau_CV-1p74_C2V-1p37_C3-14p4",
					dataset="/VBFHHto2B2Tau_CV-1p74_C2V-1p37_C3-14p4_TuneCP5_13p6TeV_madgraph-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("VBFHHto2B2Tau_CV-1p74_C2V-1p37_C3-14p4"),
					xs=0.0293703335,
					tags=["NanoAODv12"]),

			Dataset("VBFHHto2B2Tau_CV-m0p012_C2V-0p030_C3-10p2",
					dataset="/VBFHHto2B2Tau_CV-m0p012_C2V-0p030_C3-10p2_TuneCP5_13p6TeV_madgraph-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("VBFHHto2B2Tau_CV-m0p012_C2V-0p030_C3-10p2"),
					xs=0.0000009337,
					tags=["NanoAODv12"]),

			Dataset("VBFHHto2B2Tau_CV-m0p758_C2V-1p44_C3-m19p3",
					dataset="/VBFHHto2B2Tau_CV-m0p758_C2V-1p44_C3-m19p3_TuneCP5_13p6TeV_madgraph-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("VBFHHto2B2Tau_CV-m0p758_C2V-1p44_C3-m19p3"),
					xs=0.0263694193,
					tags=["NanoAODv12"]),

			Dataset("VBFHHto2B2Tau_CV-m0p962_C2V-0p959_C3-m1p43",
					dataset="/VBFHHto2B2Tau_CV-m0p962_C2V-0p959_C3-m1p43_TuneCP5_13p6TeV_madgraph-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("VBFHHto2B2Tau_CV-m0p962_C2V-0p959_C3-m1p43"),
					xs=0.0000827480,
					tags=["NanoAODv12"]),

			Dataset("VBFHHto2B2Tau_CV-m1p21_C2V-1p94_C3-m0p94",
					dataset="/VBFHHto2B2Tau_CV-m1p21_C2V-1p94_C3-m0p94_TuneCP5_13p6TeV_madgraph-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("VBFHHto2B2Tau_CV-m1p21_C2V-1p94_C3-m0p94"),
					xs=0.0002787730,
					tags=["NanoAODv12"]),

			Dataset("VBFHHto2B2Tau_CV-m1p60_C2V-2p72_C3-m1p36",
					dataset="/VBFHHto2B2Tau_CV-m1p60_C2V-2p72_C3-m1p36_TuneCP5_13p6TeV_madgraph-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("VBFHHto2B2Tau_CV-m1p60_C2V-2p72_C3-m1p36"),
					xs=0.0008586774,
					tags=["NanoAODv12"]),

			Dataset("VBFHHto2B2Tau_CV-m1p83_C2V-3p57_C3-m3p39",
					dataset="/VBFHHto2B2Tau_CV-m1p83_C2V-3p57_C3-m3p39_TuneCP5_13p6TeV_madgraph-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("VBFHHto2B2Tau_CV-m1p83_C2V-3p57_C3-m3p39"),
					xs=0.0012367629,
					tags=["NanoAODv12"]),

			Dataset("VBFHHto2B2Tau_CV-m2p12_C2V-3p87_C3-m5p96",
					dataset="/VBFHHto2B2Tau_CV-m2p12_C2V-3p87_C3-m5p96_TuneCP5_13p6TeV_madgraph-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("VBFHHto2B2Tau_CV-m2p12_C2V-3p87_C3-m5p96"),
					xs=0.0499087685,
					tags=["NanoAODv12"]),

			# Non-resonant ggHH signals
			Dataset("GluGlutoHHto2B2Tau_kl-0p00_kt-1p00_c2-0p00",
					dataset="/GluGlutoHHto2B2Tau_kl-0p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv13-133X_mcRun3_2022_realistic_postEE_ForNanov13_v1-v2/NANOAODSIM",
					process=self.processes.get("GluGlutoHHto2B2Tau_kl-0p00_kt-1p00_c2-0p00"),
					xs=0.0055322467,
					tags=["NanoAODv13"]),

			Dataset("GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00",
					dataset="/GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v3/NANOAODSIM",
					process=self.processes.get("GluGlutoHHto2B2Tau_kl-1p00_kt-1p00_c2-0p00"),
					xs=0.0024926149,
					tags=["NanoAODv12"]),

			Dataset("GluGlutoHHto2B2Tau_kl-2p45_kt-1p00_c2-0p00",
					dataset="/GluGlutoHHto2B2Tau_kl-2p45_kt-1p00_c2-0p00_LHEweights_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv12-130X_mcRun3_2022_realistic_postEE_v6-v2/NANOAODSIM",
					process=self.processes.get("GluGlutoHHto2B2Tau_kl-2p45_kt-1p00_c2-0p00"),
					xs=0.0010896518,
					tags=["NanoAODv12"]),

			Dataset("GluGlutoHHto2B2Tau_kl-5p00_kt-1p00_c2-0p00",
					dataset="/GluGlutoHHto2B2Tau_kl-5p00_kt-1p00_c2-0p00_TuneCP5_13p6TeV_powheg-pythia8/Run3Summer22EENanoAODv13-133X_mcRun3_2022_realistic_postEE_ForNanov13_v1-v3/NANOAODSIM",
					process=self.processes.get("GluGlutoHHto2B2Tau_kl-5p00_kt-1p00_c2-0p00"),
					xs=0.0072777345,
					tags=["NanoAODv13"]),

            # For GitLab CI tests
            Dataset("GluGlutoHHto2B2Tau_SM_2022_postEE_100events",
                    folder=".//test///data",
                    process=self.processes.get("GluGlutoHHto2B2Tau_SM_2022_postEE_100events"),
                    xs=0.002516833,
                    tags=["NanoAODv12"]),
            Dataset("Tau_2022_postEE_100events",
                    folder=".//test///data",
                    process=self.processes.get("Tau_2022_postEE_100events"),
                    runEra="E",
                    tags=["NanoAODv12"]),

		]
		return ObjectCollection(datasets)
